import io
import json

class ResultsManager:


    def __init__(self):
        try:
            self.resultsFileRead = open("results.json", "r")
            self.results = dict(lst=[])
            tmp = self.resultsFileRead.read()
            if tmp != "":
                #print(tmp)
                self.results = json.loads(tmp)
        except FileNotFoundError:
            self.results = dict(lst=[])

        #print(self.results)
        #print("Results init!")

    def saveNewResult(self, username, score):
        #print(username, score)
        self.results["lst"].append(dict(username=username, score=score))
        stringified = json.dumps(self.results, sort_keys=True, indent = 4, separators = (',', ': '))
        resultsFileWrite = open("results.json", "w")
        resultsFileWrite.write(stringified)

    def loadResults(self):
        return sorted(self.results["lst"], reverse=True, key=lambda k: k['score'])