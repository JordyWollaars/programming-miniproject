import time
import hashlib
import requests
import json
import random

class MarvelAPIManager:


    def __init__(self):
        self.characterList = []
        #print("MarvelAPIManager init!")

    def setupMarvelAPI(self):
        timestamp = str(time.time())
        private_key = "2415d4b440168dd2e4ad4b891a4e479ad9229db3"
        public_key = "7673043631df84d03361408971c2b060"
        hash = hashlib.md5( (timestamp+private_key+public_key).encode('utf-8') )
        md5digest = str(hash.hexdigest())
        url = "http://gateway.marvel.com:80/v1/public/characters"

        request_limit = 99

        connection_url = url+"?ts="+timestamp+"&limit="+str(request_limit)+"&apikey="+public_key+"&events=238&hash="+md5digest
        #print(connection_url)
        response = requests.get(connection_url)
        self.jsontext = json.loads(response.text)

        #print(self.jsontext["data"]["results"])

    def getCharacterInfo(self):
        for character in self.jsontext["data"]["results"]:
            newCharacter = dict(name="", description="", comics=[], series=[], stories=[])

            newCharacter["name"] = character["name"]

            newCharacter["description"] = character["description"]

            for comic in character["comics"]["items"]:
                newCharacter["comics"].append(comic["name"])

            for serie in character["series"]["items"]:
                newCharacter["series"].append(serie["name"])

            for storie in character["stories"]["items"]:
                newCharacter["stories"].append(storie["name"])

            if(newCharacter["description"] == ""):
                continue

            self.characterList.append(newCharacter)

        # print(random.randrange(len(self.characterList)))
        # print(self.characterList[random.randrange(len(self.characterList))])

    def getNewCharacterToGuess(self):
        return self.characterList.pop(random.randrange(len(self.characterList)))