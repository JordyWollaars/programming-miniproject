class QuizManager:

    def __init__(self, appManager):
        self.marvelAPI = appManager.marvelAPI
        self.appManager = appManager

        self.score = 0
        self.heroesToGuess = []
        self.SetHeroes(5)
        self.currentHero = self.SetHeroToGuess()
        self.scoreToReceive = 25
        #print("Initiated QuizManager!")
        #print("score = " + str(self.score))
        #print("heroesToGuess = " + str(self.heroesToGuess))
        print(self.currentHero["name"])

    def SetHeroes(self, length):
        for i in range(length):
            self.heroesToGuess.append(self.marvelAPI.getNewCharacterToGuess())

    def SetHeroToGuess(self):
        if len(self.heroesToGuess) == 0:
            self.EndQuiz()
            return False

        return self.heroesToGuess.pop(0)

    def EditScore(self):
        self.score += self.scoreToReceive
        self.scoreToReceive = 25

    def Penalty(self, penalty):
        self.scoreToReceive -= penalty

    def GetHint(self, key):
        self.Penalty(3)
        if key == "description":
            return self.StripString(self.currentHero[key], self.currentHero["name"])

        return self.ListNamesToString(self.currentHero[key])

    def SubmitAnswer(self, answer):
        if answer == self.currentHero["name"]:
            self.EditScore()
            self.currentHero = self.SetHeroToGuess()
            #print("Score = " + str(self.score))
            if self.currentHero != False:
                print(self.currentHero["name"])
            return True
        else:
            self.Penalty(1)
            return False
            #print("Incorrect")

    def EndQuiz(self):
        #print(self.appManager.TKinterManager)
        self.appManager.EndQuiz()
        #print("Quiz finished!")

    def GetScore(self):
        return self.score

    def StripString(self, stringToEdit, filter):
        words = stringToEdit.split()
        filteredWord = ""
        for word in words:
            if word != filter:
                filteredWord += word + " "
            else:
                filteredWord += "???????? "

        return filteredWord

    def ListNamesToString(self, lst):
        wordsToCollect = ""
        for obj in lst:
            wordsToCollect += obj + ", "

        return wordsToCollect