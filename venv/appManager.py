from MarvelAPIManager import MarvelAPIManager
from TKinterManager import TKinterManager
from quizManager import QuizManager
from ResultsManager import ResultsManager

class AppManager:

    def __init__(self):
        print("Loading...")
        print("Hulp middel bij testen:")
        #print("AppManager init!")
        self.ResultsManager = ResultsManager()
        self.marvelAPI = MarvelAPIManager()
        self.marvelAPI.setupMarvelAPI()
        self.marvelAPI.getCharacterInfo()
        self.TKinterManager = TKinterManager(self)

        self.username = ""

    def StartQuiz(self):
        self.quizManager = QuizManager(self)

    def EndQuiz(self):
        self.TKinterManager.TKinterActions.EndQuiz()
        self.TKinterManager.nextFrame = self.TKinterManager.resultsFrame
        self.ResultsManager.saveNewResult(self.username, self.quizManager.GetScore())
        self.TKinterManager.TKinterActions.ResetResults()

appManager = AppManager()