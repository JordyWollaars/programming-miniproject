from tkinter import *

class TKinterManager:

    def __init__(self, appManager):
        self.appManager = appManager
        self.appManager.TKinterManager = self
        self.TKinterActions = TKinterActions(self)

        self.root = Tk()

        self.startFrame()
        self.quizFrame()
        self.answeredFrame()
        self.resultsFrame()

        self.activeFrame = self.startFrame

        self.root.mainloop()

    def startFrame(self):
        self.startFrame = Frame(self.root)
        self.startFrame.pack()

        #Left
        self.startFrameSetup = Frame(self.startFrame)
        self.startFrameSetup.pack(side=LEFT)

        self.SFlabel = Label(self.startFrameSetup, text='Super Wonder Captain', height=2)
        self.SFlabel.pack()
        self.SFentry = Entry(self.startFrameSetup)
        self.SFentry.insert(0, "Username")
        self.SFentry.bind("<Button-1>", lambda x: self.TKinterActions.EmptyEntry(self.SFentry))
        self.SFentry.pack(padx=18, pady=18)

        self.SFbutton = Button(self.startFrameSetup, text='Start Quiz',
                             command=lambda: self.TKinterActions.SetUsername(self.SFentry))
        self.SFbutton.pack(pady=18)

        #Right
        self.startFrameResults = Frame(self.startFrame)
        self.startFrameResults.pack(side=RIGHT)

        count=1
        self.SFRLabels = []
        for result in self.appManager.ResultsManager.loadResults():

            self.SFRLabels.append(Label(self.startFrameResults, text=str(count)+": "+result["username"]+" - "+str(result["score"])))
            self.SFRLabels[count-1].pack()
            count += 1
            if count == 10:
                break

    def quizFrame(self):
        self.quizFrame = Frame(self.root)


        # LeftFrame
        self.quizFrameLeft = Frame(self.quizFrame)
        self.quizFrameLeft.pack(side=LEFT)

        self.QFlabel = Label(self.quizFrameLeft, text='Hero to guess = ??????????', height=2)
        self.QFlabel.pack()

        self.QFentry = Entry(self.quizFrameLeft)
        self.QFentry.insert(0, "Hero name")
        self.QFentry.bind("<Button-1>", lambda x: self.TKinterActions.EmptyEntry(self.QFentry))
        self.QFentry.pack(padx=18, pady=18)

        self.QFButton = Button(self.quizFrameLeft, text='Submit answer',
                               command= lambda: self.TKinterActions.SubmitAnswer(self.QFentry))
        self.QFButton.pack()


        # RightFrame
        self.quizFrameRight = Frame(self.quizFrame)
        self.quizFrameRight.pack(side=RIGHT)
        self.quizFrameButtonsRight = Frame(self.quizFrameRight)
        self.quizFrameButtonsRight.pack(side=LEFT)
        self.quizFrameHintsRight = Frame(self.quizFrameRight)
        self.quizFrameHintsRight.pack(side=RIGHT)

        self.QFButtonHint1 = Button(self.quizFrameButtonsRight, text='Description',
                                    command= lambda: self.TKinterActions.Hint("description"))
        self.QFButtonHint2 = Button(self.quizFrameButtonsRight, text='Comic',
                                    command= lambda: self.TKinterActions.Hint("comics"))
        self.QFButtonHint3 = Button(self.quizFrameButtonsRight, text='Series',
                                    command= lambda: self.TKinterActions.Hint("series"))
        self.QFButtonHint4 = Button(self.quizFrameButtonsRight, text='Stories',
                                    command= lambda: self.TKinterActions.Hint("stories"))

        self.QFButtonHint1.pack()
        self.QFButtonHint2.pack()
        self.QFButtonHint3.pack()
        self.QFButtonHint4.pack()

        self.QFLabel1 = Label(self.quizFrameHintsRight, text="Unknown", anchor=W, justify=LEFT)
        self.QFLabel2 = Label(self.quizFrameHintsRight, text="Unknown", anchor=W, justify=LEFT)
        self.QFLabel3 = Label(self.quizFrameHintsRight, text="Unknown", anchor=W, justify=LEFT)
        self.QFLabel4 = Label(self.quizFrameHintsRight, text="Unknown", anchor=W, justify=LEFT)

        self.QFLabels = dict(description=self.QFLabel1, comics=self.QFLabel2, series=self.QFLabel3,
                             stories=self.QFLabel4)

        self.QFLabel1.pack()
        self.QFLabel2.pack()
        self.QFLabel3.pack()
        self.QFLabel4.pack()

        #self.QFSubmitButton = Button(self.quizFrame, text='Submit',
                                     #command=lambda: self.TKinterActions.SubmitAnswer(self.QFentry))

    def resetQuizFrame(self):
        for label in self.QFLabels:
            #print(label)
            self.QFLabels[label].config(text="Unknown")

    def answeredFrame(self):
        self.answeredFrame = Frame(self.root)

        self.AFLabel = Label(self.answeredFrame, text="Placeholder")
        self.AFLabel.pack()
        self.AFButton = Button(self.answeredFrame, text="Ok!",
                               command= lambda: self.TKinterActions.ChangeFrame(self.quizFrame))
        self.AFButton.pack()

        self.nextFrame = self.answeredFrame

    def resultsFrame(self):
        self.resultsFrame = Frame(self.root)

        self.RFLabel = Label(self.resultsFrame, text="Total score = " + str(0))
        self.RFLabel.pack()

        self.RFRestartButton = Button(self.resultsFrame, text="Restart", command=self.TKinterActions.Restart)
        self.RFRestartButton.pack()

        self.RFQuitButton = Button(self.resultsFrame, text="Quit", command=self.root.destroy)
        self.RFQuitButton.pack()

class TKinterActions:

    def __init__(self, TKinterManager):
        self.TKinterManager = TKinterManager

    def ResetResults(self):
        for label in self.TKinterManager.SFRLabels:
            label.pack_forget()

        count = 1
        self.TKinterManager.SFRLabels = []
        for result in self.TKinterManager.appManager.ResultsManager.loadResults():

            self.TKinterManager.SFRLabels.append(Label(self.TKinterManager.startFrameResults,
                                        text=str(count) + ": " + result["username"] + " - " + str(result["score"])))
            self.TKinterManager.SFRLabels[count - 1].pack()
            count += 1
            if count == 10:
                break

    def GetEntryValue(self, entry):
        return entry.get()

    def SetUsername(self, entry):
        self.TKinterManager.appManager.username = self.GetEntryValue(entry)
        self.StartQuiz()
        self.ChangeFrame(self.TKinterManager.quizFrame)

    def SubmitAnswer(self, entry):
        self.AnswerComfimation(self.TKinterManager.appManager.quizManager.SubmitAnswer(self.GetEntryValue(entry)))

    def StartQuiz(self):
        self.TKinterManager.appManager.StartQuiz()

    def EndQuiz(self):
        self.TKinterManager.RFLabel.config(
            text="Total score = "+str(self.TKinterManager.appManager.quizManager.GetScore()))

    def ChangeFrame(self, targetFrame):
        targetFrame.pack()
        self.TKinterManager.activeFrame.pack_forget()
        self.TKinterManager.activeFrame = targetFrame

    def Hint(self, key):
        value = self.TKinterManager.appManager.quizManager.GetHint(key)
        self.TKinterManager.QFLabels[key].config(text=value)

    def EmptyEntry(self, entry):
        #print("Call!")
        entry.delete(0, END)
        entry.insert(0, "")

    def AnswerComfimation(self, check):
        value = StringVar()
        if check == True:
            value = "Correct answer!"
        else:
            value = "Incorrect answer. Please try again!"

        self.TKinterManager.AFLabel.config(text=value)
        self.ChangeFrame(self.TKinterManager.nextFrame)
        self.TKinterManager.resetQuizFrame()

    def Restart(self):
        self.ChangeFrame(self.TKinterManager.startFrame)
        self.TKinterManager.nextFrame = self.TKinterManager.answeredFrame